DROP TYPE albania_tpg_data CASCADE;
CREATE TYPE albania_tpg_data AS (
  --Included all Albanian keys, since I don't know what they mean/which are important
	gid integer,
	the_geom geometry,
	id numeric,
	way_type text,
	category smallint,
	cat2 smallint,
	shape_leng numeric,
	way_name text,
	shape_le_1 numeric,
	et_id integer

);

DROP TYPE albania_tpg_data_area CASCADE;
CREATE TYPE albania_tpg_data_area AS (
  --Included all Albanian keys, since I don't know what they mean/which are important
	gid integer,
	the_geom geometry,
	id numeric,
	way_type text,
	category smallint,
	cat2 smallint,
	shape_leng numeric,
	way_name text,
	shape_le_1 numeric,
	et_id integer

);

CREATE OR REPLACE FUNCTION select_albania_tpg_roadtile_area(areaid integer) 
RETURNS SETOF albania_tpg_data_area
AS $$
SELECT 
	gid, --1 
	the_geom, --2
	id , --3
	"type", 
        cast(category as smallint),--5
	cast(cat2 as smallint), --6
	shape_leng, --7
	albania_tpg_roadseg.name, --8
	shape_le_1, --9
	cast(et_id as integer)  --10

  FROM tpginccommunes_polygon 
  join albania_tpg_roadseg
on   tpginccommunes_polygon.way &&  albania_tpg_roadseg.the_geom
and
--  st_intersects(albania_tpg_roadseg.the_geom, tpginccommunes_polygon.way)
--  convers(tpginccommunes_polygon.way,albania_tpg_roadseg.the_geom )
--  contains(tpginccommunes_polygon.way,albania_tpg_roadseg.the_geom )
(  touches(tpginccommunes_polygon.way,albania_tpg_roadseg.the_geom )
or 
  contains(tpginccommunes_polygon.way,albania_tpg_roadseg.the_geom )
)
  where osm_id = $1
$$
LANGUAGE SQL;


CREATE OR REPLACE FUNCTION select_albania_tpg_roadtile(coordinates text) 
RETURNS SETOF albania_tpg_data
AS $$
	SELECT 
	gid, --1 
	the_geom, --2
	id , --3
	"type", --4might need 4191 rather than 4326
        cast(category as smallint),--5
	cast(cat2 as smallint), --6
	shape_leng, --7
	"name", --8
	shape_le_1, --9
	cast(et_id as integer)  --10
FROM albania_tpg_roadseg 
WHERE
    ST_Intersects(ST_Transform(the_geom,4326),
                     ST_GeomFromEWKT($1))

		
$$
LANGUAGE SQL;

DROP TYPE albania_osm_data CASCADE;
CREATE TYPE albania_osm_data AS (	
	way geometry,
	osm_id integer,
	name text
);

CREATE OR REPLACE FUNCTION select_albania_osm_roadtile(coordinates text) 
RETURNS SETOF albania_osm_data
AS $$
	SELECT way,
	       osm_id,substr(name,0,80)
                FROM planet_osm_line WHERE
		ST_Intersects(way,ST_GeomFromEWKT($1)) 		
 	        AND ((highway is not null)
 	          and(railway is not null)) --Albania appears to have railways in gov_data
$$
LANGUAGE SQL;



CREATE OR REPLACE FUNCTION select_albania_osm_roadtile_area(areaid integer) 
RETURNS SETOF albania_osm_data
AS $$
	SELECT ST_Transform(planet_osm_line.way,4326),
	       planet_osm_line.osm_id,
	       substr(planet_osm_line.name,0,80)

        FROM 
	     tpginccommunes_polygon 
	join 
		planet_osm_line
on   tpginccommunes_polygon.way &&  planet_osm_line.way
and
	(
contains(tpginccommunes_polygon.way,planet_osm_line.way )
or
	touches(tpginccommunes_polygon.way,planet_osm_line.way )
)
  where tpginccommunes_polygon.osm_id = $1
$$
LANGUAGE SQL;


CREATE OR REPLACE FUNCTION select_albania_osm_roadtile2_area(areaid integer) 
RETURNS SETOF albania_osm_data
AS $$
	SELECT ST_Transform(planet_osm_roads.way,4326),
	   planet_osm_roads.osm_id,
   	  substr(planet_osm_roads.name,0,80) 
        FROM tpginccommunes_polygon 
		join 
		planet_osm_roads
on   tpginccommunes_polygon.way &&  planet_osm_roads.way
and
(
  contains(tpginccommunes_polygon.way,planet_osm_roads.way )
or
  touches(tpginccommunes_polygon.way,planet_osm_roads.way )
)
  where tpginccommunes_polygon.osm_id = $1
$$
LANGUAGE SQL;


-- test
--select * from  select_albania_osm_roadtile2_area(-81154);
--select * from  select_albania_osm_roadtile_area(-81154);