use strict;
use warnings;
#use CWD;
use Cwd;
my $cwd = getcwd;
my $file =shift;
$file =~ /([a-zA-Z0-9_]+)/;
my $name= $1;

print qq[
  LAYER
    NAME "${name}"
    STATUS DEFAULT
    TYPE RASTER
    DATA $cwd/${file}
   END
];
